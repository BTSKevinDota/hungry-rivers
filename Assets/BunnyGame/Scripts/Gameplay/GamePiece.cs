﻿using System.Collections;
using BunnyGame.Scripts.Bunny;
using BunnyGame.Scripts.Static;
using UnityEngine;

namespace BunnyGame.Scripts.Gameplay
{
    public class GamePiece : MonoBehaviour
    {
        #region Variables
        
        //To which player does the piece belong.        
        public PlayerBase player;

        //Sprite of the piece.
        public Sprite sprite;

        //Has this piece ever collided with another food or with the island?
        public bool hasCollidedOnce;

        //RectTransform-Component
        private RectTransform _rectTransform;

        //RigidBody-Component
        public Rigidbody2D rigidBody;

        //ConstantForce-Component
        private ConstantForce2D _constantForce;

        //Is the food inside the limit-boundaries?
        public bool insideLimit;

        public PolygonCollider2D collisionCollider;

        public PhysicsMaterial2D friction;
        public PhysicsMaterial2D noFriction;

        public Island island;

        public int points;

        public float fitFactor;

        public bool isExploding;

        private float _gravityMultiplier;

        private const bool FreezeRotationZ = false;
                
        private Vector2 _initialSize;
        
        #endregion
        
        private void Awake()
        {            
            enabled = false;
            
            _gravityMultiplier = Screen.height * 0.375f * -9.81f;

            _rectTransform = GetComponent<RectTransform>();

            _initialSize = sprite.rect.size;
            _rectTransform.sizeDelta = new Vector2(Screen.height * 0.25f / fitFactor, Screen.height * 0.25f / fitFactor) * TextureRatio(_initialSize);

            rigidBody = GetComponent<Rigidbody2D>();
            rigidBody.sharedMaterial = noFriction;
            _constantForce = GetComponent<ConstantForce2D>();

            collisionCollider.sharedMaterial = noFriction;
            rigidBody.freezeRotation = FreezeRotationZ;

            island = GameObject.FindWithTag("Island").GetComponent<Island>();

            
            var colliderPathLength = collisionCollider.GetPath(0).Length;
            var newCollider = new Vector2[colliderPathLength];

            for (var i = 0; i < colliderPathLength; i++)
            {
                newCollider[i] = new Vector2
                (
                    collisionCollider.GetPath(0)[i].x / (_initialSize / _rectTransform.sizeDelta).x,
                    collisionCollider.GetPath(0)[i].y / (_initialSize / _rectTransform.sizeDelta).y
                );
            }

            collisionCollider.SetPath(0, newCollider);
            collisionCollider.offset = Vector2.zero;
        }

        private void Start()
        {
            hasCollidedOnce = false;
            insideLimit = false;
            isExploding = false;
        }

        private void FixedUpdate()
        {
            if (!hasCollidedOnce)
            {
                return;
            }

             Vector2 gravityUp = (transform.position - island.transform.position).normalized;

             
            ApplyForce(gravityUp * _gravityMultiplier * Time.deltaTime);
        }

        public void SetOwner(PlayerBase playerBase)
        {
            _rectTransform.pivot = playerBase.gameArea.pivot;
            player = playerBase;
            enabled = true;
        }

        private static Vector2 TextureRatio(Vector2 texture)
        {
            var textureSize = new Vector2(texture.x, texture.y);

            return textureSize.x > textureSize.y
                ? new Vector2(textureSize.x / textureSize.y, 1)
                : new Vector2(1, textureSize.y / textureSize.x);
        }

        public IEnumerator ExpandAndExplode()
        {
            GameState.ReduceHealth();
            isExploding = true;

            for (var i = 0; i < 5; i++)
            {
                _rectTransform.localScale -= new Vector3(0.12f, 0.12f, 0.12f);
                yield return new WaitForSeconds(0.1f);
            }

            Destroy(gameObject);
            yield return null;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            var gamePiece = other.gameObject.GetComponent<GamePiece>();

            if (ReferenceEquals(gamePiece, null) || !insideLimit || hasCollidedOnce || !gamePiece.hasCollidedOnce || 
                !gamePiece.insideLimit && insideLimit || gamePiece.insideLimit && !insideLimit)
            {
                return;
            }

            hasCollidedOnce = true;
            GameState.GameScore += points * GameState.CurrentScoreMultiplier;        
            
            foreach (var collision in FindObjectsOfType<CustomCollider>())
            {
                Physics2D.IgnoreCollision(collisionCollider, collision.GetComponent<BoxCollider2D>(), true);
            }

            player.RemoveGamePiece();
            collisionCollider.sharedMaterial = friction;

            GamePieceSpawner.SpawnFood(player);
        }


        public void ApplyForce(Vector2 force)
        {
            if (ReferenceEquals(_constantForce, null))
            {
                return;
            }

            _constantForce.force = force;
        }

        public void SetVelocity(Vector2 velocity)
        {
            rigidBody.velocity = velocity;
        }

        public Vector2 GetVelocity()
        {
            return rigidBody.velocity;
        }
    }
}