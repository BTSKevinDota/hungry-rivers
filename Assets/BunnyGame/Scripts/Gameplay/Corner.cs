﻿using BunnyGame.Scripts.Static;
using UnityEngine;

namespace BunnyGame.Scripts.Gameplay
{
    public class Corner : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            var gamePiece = other.GetComponent<GamePiece>();

            if (ReferenceEquals(gamePiece, null))
            {
                return;
            }

            if (!gamePiece.hasCollidedOnce || gamePiece.isExploding)
            {
                return;
            }
            
            GameState.GameScore -= gamePiece.points * GameState.CurrentScoreMultiplier;
            StartCoroutine(gamePiece.ExpandAndExplode());
        }
    }
}