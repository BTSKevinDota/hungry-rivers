﻿using System;
using System.IO;
using BunnyGame.Scripts.Highscore;
using BunnyGame.Scripts.Static;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Logic
{
    public class EndGameController : MonoBehaviour
    {
        private string _filePath;
        public HighScore highScoreSave;

        public TextMeshProUGUI endScore;
        
        public GameObject endScreen;
        public GameObject enterTeamName;

        public int indexFound;

        public Button menu;

        public bool endScreenIsActive;

        private bool _replace;

        public AudioSource source;
        
        private void Start()
        {
            endScreen.SetActive(false);
            enterTeamName.SetActive(false);
                        
            LoadHighScore(GameState.PlayerAmount);
            BubbleSort();
            
            _replace = false;
            endScreenIsActive = false;
            indexFound = -1;

            endScore.text = "End Score: " + Convert.ToInt32(GameState.GameScore);
            
            if (highScoreSave.highScoreList.Count + 1 <= 10)
            {
                enterTeamName.SetActive(true);
                source.Play();
                return;
            }

            for (var i = 0; i < highScoreSave.highScoreList.Count; i++)
            {
                if (GameState.GameScore > highScoreSave.highScoreList[i].teamScore)
                {
                    indexFound = i;
                }
            }
            
            if (indexFound != -1)
            {
                source.Play();
                enterTeamName.SetActive(true);
                _replace = true;
                return;
            }

            endScreen.SetActive(true);
            endScreenIsActive = true;
        }


        private void Replace(string teamName, int score)
        {
            if (indexFound == -1)
            {
                return;
            }

           highScoreSave.highScoreList.Insert(indexFound, new HighScore.Score(teamName, score));
           highScoreSave.highScoreList.RemoveAt(0);

        }
        
        public void AddScore(string teamName, int score)
        {
            if (_replace)
            {
                Replace(teamName, score);
            }
            else
            {
                highScoreSave.highScoreList.Add(new HighScore.Score(teamName, score));
            }
           
            BubbleSort();
            SaveHighScore();
            
            endScreen.SetActive(true);
            enterTeamName.SetActive(false);
        }
        
        private void LoadHighScore(int playerAmount)
        {
            _filePath = Application.dataPath + "/" + playerAmount + "_Player_HighScore.json";        
        
            if (File.Exists(_filePath))
            {
                var dataAsJson = File.ReadAllText(_filePath);
                highScoreSave = JsonUtility.FromJson<HighScore>(dataAsJson);
            }
            else
            {
                SaveHighScore();
                
                var dataAsJson = File.ReadAllText(_filePath);
                highScoreSave = JsonUtility.FromJson<HighScore>(dataAsJson);
            }
        }
        
        private void SaveHighScore()
        {
            var dataAsJson = JsonUtility.ToJson (highScoreSave);
            File.WriteAllText (_filePath, dataAsJson);
        }

        private void BubbleSort()
        {
            for (var i = 0; i < highScoreSave.highScoreList.Count; i++) 
            {
                for (var j = 0; j < highScoreSave.highScoreList.Count - 1; j++) 
                {
                    if (highScoreSave.highScoreList[j].teamScore <= highScoreSave.highScoreList[j + 1].teamScore)
                    {
                        continue;
                    }
                    
                    var temp = highScoreSave.highScoreList[j + 1];
                    highScoreSave.highScoreList[j + 1] = highScoreSave.highScoreList[j];
                    highScoreSave.highScoreList[j] = temp;
                }
            }
        }
        
        public void GoToMenu()
        {
            SceneManager.LoadScene("Menu");
        }
    
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha7) && endScreenIsActive)
            {
                menu.onClick.Invoke();
            }
        }
    }
}