﻿using BunnyGame.Scripts.Bunny;
using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BunnyGame.Scripts.Logic
{
    public class GameController : MonoBehaviour
    {
        public float seconds = 5f;

        private void Awake()
        {           
            Players.PlayerOne = FindObjectOfType<PlayerOne>();
            Players.PlayerTwo = FindObjectOfType<PlayerTwo>();
            Players.PlayerThree = FindObjectOfType<PlayerThree>();
            Players.PlayerFour = FindObjectOfType<PlayerFour>();

            Players.PlayerOne.enabled = false;
            Players.PlayerTwo.enabled = false;
            Players.PlayerThree.enabled = false;
            Players.PlayerFour.enabled = false;

            switch (GameState.PlayerAmount)
            {
                case 2:
                    Players.PlayerOne.enabled = true;
                    Players.PlayerTwo.enabled = true;
                    
                    GameObject.FindWithTag("PowerUpCooldown1").SetActive(true);
                    GameObject.FindWithTag("PowerUpCooldown2").SetActive(true);
                    GameObject.FindWithTag("PowerUpCooldown3").SetActive(false);
                    GameObject.FindWithTag("PowerUpCooldown4").SetActive(false);
                    break;
                
                case 4:
                    Players.PlayerOne.enabled = true;
                    Players.PlayerTwo.enabled = true;
                    Players.PlayerThree.enabled = true;
                    Players.PlayerFour.enabled = true;
                    
                    GameObject.FindWithTag("PowerUpCooldown1").SetActive(true);
                    GameObject.FindWithTag("PowerUpCooldown2").SetActive(true);
                    GameObject.FindWithTag("PowerUpCooldown3").SetActive(true);
                    GameObject.FindWithTag("PowerUpCooldown4").SetActive(true);
                    break;
                
                default:
                    Players.PlayerOne.enabled = true;
                    Players.PlayerTwo.enabled = true;
                    Players.PlayerThree.enabled = true;
                    Players.PlayerFour.enabled = true;
                    break;
            }
        }

        private void Update()
        {
            if (GameState.GameLives < 0)
            {
                SceneManager.LoadScene("End Game");
                return;
            }
            
            if (Players.PlayerOne.timeSinceLastSpawn < seconds)
            {
                return;
            }
            
            if (Players.PlayerTwo.timeSinceLastSpawn < seconds)
            {
                return;
            }
            
            if (Players.PlayerThree.timeSinceLastSpawn < seconds)
            {
                return;
            }
            
            if (Players.PlayerFour.timeSinceLastSpawn < seconds)
            {
                return;
            }

            GameState.CooldownOne = Players.PlayerOne.powerUp.timer;
            GameState.CooldownTwo = Players.PlayerTwo.powerUp.timer;
            GameState.CooldownThree = Players.PlayerThree.powerUp.timer;
            GameState.CooldownFour = Players.PlayerFour.powerUp.timer;

            GameState.NextLevel();
        }
    }
}