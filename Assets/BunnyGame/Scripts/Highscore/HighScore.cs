﻿using System.Collections.Generic;

namespace BunnyGame.Scripts.Highscore
{
    [System.Serializable]
    public class HighScore
    {
        [System.Serializable]
        public struct Score
        {
            public string teamName;
            public int teamScore;

            public Score(string teamName, int teamScore)
            {
                this.teamName = teamName;
                this.teamScore = teamScore;
            }
        } 
                
        public List<Score> highScoreList;

        public Score GetScore(int index)
        {
            return highScoreList[index];
        }
        
        public int GetLength()
        {
            return highScoreList.Count;
        }

        public void Sort()
        {
            for (var j = 0; j <= highScoreList.Count - 2; j++) 
            {
                for (var i = 0; i <= highScoreList.Count - 2; i++) 
                {
                    if (highScoreList[i].teamScore >= highScoreList[i + 1].teamScore)
                    {
                        continue;
                    }
                    
                    var temp = highScoreList[i + 1];
                    highScoreList[i + 1] = highScoreList[i];
                    highScoreList[i] = temp;
                }
            }

        }
    }
}