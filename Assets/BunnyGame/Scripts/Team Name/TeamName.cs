﻿using BunnyGame.Scripts.Logic;
using BunnyGame.Scripts.Static;
using BunnyGame.Scripts.Team_Name;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts
{
    public class TeamName : MonoBehaviour
    {
        public string teamName;

        public TeamLetter[] Letters;
        public TeamLetter Selected;

        public Color32 SelectedColor;
        public Color32 DeselectedColor;

        public Button Submit;
        public ColorBlock SelectButton;
        public ColorBlock DeSelectButton;
        public bool SubmitIsSelected;
 
        public bool HorizontalIsReady;
        public bool VerticalIsReady;
    
        private float _horizontalTimer;
        private float _verticalTimer;

        public GameObject EndScreen;
        public EndGameController EndGameController;

        public AudioSource source;
        public AudioClip clip;
        
        private const float _resetTime = 0.2f;

        private int _index;
    
        private void Start()
        {
            Letters = GetComponentsInChildren<TeamLetter>();
            _index = 0;
            Selected = Letters[_index];
            Selected.ChangeColor(SelectedColor);

            SubmitIsSelected = false;
        
            _horizontalTimer = 0;
            _verticalTimer = 0;   
        }

        private void Update()
        {
            if (_horizontalTimer > 0)
            {
                _horizontalTimer -= Time.deltaTime;
                HorizontalIsReady = false;
            }
            else
            {
                HorizontalIsReady = true;
            }
        
            if (_verticalTimer > 0)
            {
                _verticalTimer -= Time.deltaTime;
                VerticalIsReady = false;
            }
            else
            {
                VerticalIsReady = true;
            }
              
            if (Input.GetAxisRaw("Horizontal1") > 0 && HorizontalIsReady)
            {
                source.PlayOneShot(clip);
                _index++;

                Submit.colors = DeSelectButton;
                SubmitIsSelected = false;
                
                if (_index == Letters.Length)
                {
                    Submit.colors = SelectButton;
                    SubmitIsSelected = true;
                
                    Selected.ChangeColor(DeselectedColor);
                }
                else if (_index == Letters.Length + 1)
                {
                    Selected.ChangeColor(DeselectedColor);
                    Selected = Letters[0];
                    Selected.ChangeColor(SelectedColor);
                    
                    _index = 0;    
                }
                else
                {
                    Selected.ChangeColor(DeselectedColor);
                    Selected = Letters[_index];
                    Selected.ChangeColor(SelectedColor);
                }
            
                _horizontalTimer = _resetTime;   
            }
            else if (Input.GetAxisRaw("Horizontal1") < 0 && HorizontalIsReady)
            {
                source.PlayOneShot(clip);
                _index--;

                Submit.colors = DeSelectButton;
                SubmitIsSelected = false;
                
                if (_index == -1)
                {
                    Submit.colors = SelectButton;
                    SubmitIsSelected = true;
                
                    Selected.ChangeColor(DeselectedColor);
                }
                else if (_index == -2)
                {
                    Selected.ChangeColor(DeselectedColor);
                    Selected = Letters[Letters.Length - 1];
                    Selected.ChangeColor(SelectedColor);
                
                    _index = Letters.Length - 1;
                }
                else
                {
                    Selected.ChangeColor(DeselectedColor);
                    Selected = Letters[_index];
                    Selected.ChangeColor(SelectedColor);
                }
            
                _horizontalTimer = _resetTime;    
            }
        
            if (Input.GetAxisRaw("Vertical1") > 0 && VerticalIsReady)
            {
                source.PlayOneShot(clip);
                Selected.NextLetter();
                ResetTime();
            }
            else if (Input.GetAxisRaw("Vertical1") < 0 && VerticalIsReady)
            {
                source.PlayOneShot(clip);
                Selected.PreviousLetter();
                ResetTime();
            }

            if (Input.GetKeyDown(KeyCode.Alpha7) && SubmitIsSelected)
            {
                SubmitTeamName();
                EndGameController.endScreenIsActive = true;
                EndGameController.AddScore(teamName, (int)GameState.GameScore);
            }
        }

        private void ResetTime()
        {
            _verticalTimer = _resetTime;
        }

        private void SubmitTeamName()
        {
            teamName = "";

            foreach (var letter in Letters)
            {
                teamName += letter.letter;
            }
        }
    }
}