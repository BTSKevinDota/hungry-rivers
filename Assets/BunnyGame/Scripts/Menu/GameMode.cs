﻿using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Menu
{
    public class GameMode : MonoBehaviour
    {
        private Button _modeButton;
        public int playerAmount;

        private void Start()
        {
            _modeButton = GetComponent<Button>();
            _modeButton.onClick.AddListener(EnterMode);
        }

        private void EnterMode()
        {         
            GameState.NewGame(playerAmount);
        }
    }
}