﻿using System;
using System.IO;
using BunnyGame.Scripts.Display;
using BunnyGame.Scripts.Highscore;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Menu
{
    public class MenuController : MonoBehaviour
    {
        public Button firstSelected;
        private Button _lastSelected;
        private Button _selectedButton;

        public AudioSource source;
        public AudioClip clip;

        [Header("Active Button")]
        public ColorBlock activeButton;
        
        [Header("Not Active Button")]
        public ColorBlock notActiveButton;

        public float delay;
        private float _timer;


        public HighScoreDisplay highScorePrefab;

        public Transform twoPlayerContainer;
        public Transform fourPlayerContainer;
               
        public GameObject highscoreDisplay;
        private bool _highscoreDisplayIsActive;
        
        public GameObject settingsDisplay;
        private bool _settingsDisplayIsActive; 
       
        public GameObject creditsDisplay;
        private bool _creditsDisplayIsActive;

        public Slider volumeSlider;
        private bool _pressedPreviously;
        
        private void Start()
        {
            _highscoreDisplayIsActive = false;
            _settingsDisplayIsActive = false;
            _creditsDisplayIsActive = false;
            
            _lastSelected = firstSelected;
            _selectedButton = firstSelected;            
            ChangeColor();
            _timer = 0;

            Cursor.visible = false;
        }

        private void Update()
        {
            GetInput();

            if (_settingsDisplayIsActive)
            {
                if (Input.GetAxisRaw("Horizontal1") > 0)
                {
                    volumeSlider.value += 0.01f;
                }
                
                if (Input.GetAxisRaw("Horizontal1") < 0)
                {
                    volumeSlider.value -= 0.01f;
                }

                AudioListener.volume = volumeSlider.value;
            }
            
            if (_timer > 0)
            {
                _timer -= Time.deltaTime;
                return;
            }

            var direction = Input.GetAxisRaw("Vertical1");

            if (Math.Abs(direction) < 0.3f || _settingsDisplayIsActive || _creditsDisplayIsActive ||_highscoreDisplayIsActive)
            {
                return;
            }
                                    
            if (direction > 0)
            {
                source.PlayOneShot(clip);
                _lastSelected = _selectedButton;
                _selectedButton = _selectedButton.navigation.selectOnDown.GetComponent<Button>();            
                ChangeColor();
            }
            else
            {
                source.PlayOneShot(clip);
                _lastSelected = _selectedButton;
                _selectedButton = _selectedButton.navigation.selectOnUp.GetComponent<Button>();                
                ChangeColor();
            }

            _timer = delay;
        }


        private void GetInput()
        {
            if (Input.GetKeyDown(KeyCode.Alpha7) && !_settingsDisplayIsActive && !_creditsDisplayIsActive && !_highscoreDisplayIsActive)
            {
                _selectedButton.onClick.Invoke();
                _pressedPreviously = true;
            }

            if (_settingsDisplayIsActive && !_pressedPreviously)
            {
                if (Input.GetKeyDown((KeyCode.Alpha7)))
                {
                    settingsDisplay.SetActive(false);
                    _settingsDisplayIsActive = false;
                }
            }
            
            if (_creditsDisplayIsActive && !_pressedPreviously)
            {
                if (Input.GetKeyDown((KeyCode.Alpha7)))
                {
                    creditsDisplay.SetActive(false);
                    _creditsDisplayIsActive = false;  
                }
            }
            
            if (_highscoreDisplayIsActive && !_pressedPreviously)
            {
                if (Input.GetKeyDown((KeyCode.Alpha7)))
                {
                    highscoreDisplay.SetActive(false);
                    _highscoreDisplayIsActive = false;

                    foreach (Transform highScore in twoPlayerContainer)
                    {
                        Destroy(highScore.gameObject);
                    }
                
                    foreach (Transform highScore in fourPlayerContainer)
                    {
                        Destroy(highScore.gameObject);
                    }
                }
            }

            _pressedPreviously = false;
        }
        
        private void ChangeColor()
        {
            _lastSelected.colors = notActiveButton;
            _selectedButton.colors = activeButton;
        }

        public void EnterHighscore()
        {
            if (_highscoreDisplayIsActive)
            {
                return;
            }
            
            highscoreDisplay.SetActive(true);
            _highscoreDisplayIsActive = true;
            
            if (File.Exists(Application.dataPath + "/2_Player_HighScore.json"))
            {
                var dataAsJson = File.ReadAllText(Application.dataPath + "/2_Player_HighScore.json");                
                var highScoreSave = JsonUtility.FromJson<HighScore>(dataAsJson);
                highScoreSave.Sort();

                var addMore = 10 - highScoreSave.GetLength();

                for (var i = 0; i < highScoreSave.GetLength(); i++)
                {
                    var score = highScoreSave.GetScore(i);
                    var displayScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, twoPlayerContainer);
            
                    if (score.teamName != "")
                    {
                        displayScore.rank.text = (i + 1).ToString();
                        displayScore.teamName.text = score.teamName;
                        displayScore.score.text = score.teamScore.ToString();
                    }
                }

                if (addMore > 0)
                {
                    for (var i = 0; i < addMore; i++)
                    {
                        var displayScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, twoPlayerContainer);
                        displayScore.rank.text = "-";
                        displayScore.teamName.text = "-";
                        displayScore.score.text = "-";
                    }
                }  
            }
            else
            {
                for (var i = 0; i < 10; i++)
                {
                    var addHighScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, twoPlayerContainer);

                    addHighScore.rank.text = "-";
                    addHighScore.teamName.text = "-";
                    addHighScore.score.text = "-";
                }                                                
            }
                        
            if (File.Exists(Application.dataPath + "/4_Player_HighScore.json"))
            {
                var dataAsJson = File.ReadAllText(Application.dataPath + "/4_Player_HighScore.json");                
                var highScoreSave = JsonUtility.FromJson<HighScore>(dataAsJson);
                highScoreSave.Sort();
                var addMore = 10 - highScoreSave.GetLength();

                for (var i = 0; i < highScoreSave.GetLength(); i++)
                {
                    var score = highScoreSave.GetScore(i);
                    var displayScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, fourPlayerContainer);

                    if (score.teamName != "")
                    {                                
                        displayScore.rank.text = (i + 1).ToString();
                        displayScore.teamName.text = score.teamName;
                        displayScore.score.text = score.teamScore.ToString();
                    }
                }

                if (addMore > 0)
                {
                    for (var i = 0; i < addMore; i++)
                    {
                        var displayScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, fourPlayerContainer);
                        displayScore.rank.text = "-";
                        displayScore.teamName.text = "-";
                        displayScore.score.text = "-";
                    }
                } 
            }
            else
            {
                for (var i = 0; i < 10; i++)
                {
                    var addHighScore = Instantiate(highScorePrefab, Vector3.zero, Quaternion.identity, fourPlayerContainer);

                    addHighScore.rank.text = "-";
                    addHighScore.teamName.text = "-";
                    addHighScore.score.text = "-";
                }
            }
        }

        public void EnterSettings()
        {
            if (_settingsDisplayIsActive)
            {
                return;
            }
            
            settingsDisplay.SetActive(true);
            _settingsDisplayIsActive = true;
        }

        public void EnterCredits()
        {
            if (_creditsDisplayIsActive)
            {
                return;
            }
            
            creditsDisplay.SetActive(true);
            _creditsDisplayIsActive = true;
        }     
    }
}