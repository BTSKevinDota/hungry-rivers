﻿using System;
using UnityEngine;

namespace BunnyGame.Scripts.Display
{
    public class WaterSlide : MonoBehaviour
    {
        public enum Purpose
        {
            Waterfall,
            Sewer,
            Menu
        }
    
        public enum Direction
        {
            Bottom,
            Top,
            Left,
            Right
        }

        public Direction direction;
        public Purpose purpose;    
    
        private float _timer;
        private RectTransform _slider;
        private float _speed;

        public bool isOpposite;
    

        private void Start()
        {
            _slider = GetComponent<RectTransform>();

            _timer = 4;
        
            switch (purpose)
            {
                case Purpose.Waterfall:
                    _speed = (1017 - 337.5f) / _timer;
                    break;
                case Purpose.Sewer:
                    _speed = (256.5f - 31.5f) / _timer;
                    break;
                
                case Purpose.Menu:
                    _speed = (2700 - 900) / 8;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }       
        }

        private void Update()
        {                
            if (_timer > 0)
            {
                switch (purpose)
                {
                    case Purpose.Waterfall:
                        switch (direction)
                        {
                            case Direction.Bottom:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, 1) * Time.deltaTime;
                                break;
                            case Direction.Top:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, -1) * Time.deltaTime;
                                break;
                            case Direction.Left:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(1, 0) * Time.deltaTime;
                                break;
                            case Direction.Right:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(-1, 0) * Time.deltaTime;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                
                    case Purpose.Sewer:
                        switch (direction)
                        {
                            case Direction.Bottom:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, -1) * Time.deltaTime;
                                break;
                            case Direction.Top:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, 1) * Time.deltaTime;
                                break;
                            case Direction.Left:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(-1, 0) * Time.deltaTime;
                                break;
                            case Direction.Right:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(1, 0) * Time.deltaTime;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    
                    case Purpose.Menu:
                        switch (direction)
                        {
                            case Direction.Bottom:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, 1) * Time.deltaTime;
                                break;
                            case Direction.Top:
                                _slider.anchoredPosition += Vector2.up * _speed * new Vector2(0, -1) * Time.deltaTime;
                                break;
                            case Direction.Left:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(1, 0) * Time.deltaTime;
                                break;
                            case Direction.Right:
                                _slider.anchoredPosition += Vector2.right * _speed * new Vector2(-1, 0) * Time.deltaTime;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
       
                _timer -= Time.deltaTime;
                return;
            }

            switch (purpose)
            {
                case Purpose.Waterfall:
                    switch (direction)
                    {
                        case Direction.Bottom:
                            _slider.anchoredPosition = new Vector2(0, 337.5f);
                            break;
                        case Direction.Top:
                            _slider.anchoredPosition = new Vector2(0, -337.5f);
                            break;
                        case Direction.Left:
                            _slider.anchoredPosition = new Vector2(337.5f, 0);
                            break;
                        case Direction.Right:
                            _slider.anchoredPosition = new Vector2(-337.5f, 0);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }       
                    _timer = 4;
                    break;
            
                case Purpose.Sewer:
                    switch (direction)
                    {
                        case Direction.Bottom:
                            _slider.anchoredPosition = isOpposite ? new Vector2(15.75f, -144) : new Vector2(-15.75f, -144);  
                            break;
                        case Direction.Top:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(15.75f, 144) : new Vector2(-15.75f, 144);
                            break;
                        case Direction.Left:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(-144, 15.75f) : new Vector2(-144, -15.75f); 
                            break;
                        case Direction.Right:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(144, 15.75f) : new Vector2(144f, -15.75f);                        
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }     
                    _timer = 4;
                    break;
                
                case Purpose.Menu:
                    switch (direction)
                    {
                        case Direction.Bottom:
                            _slider.anchoredPosition = isOpposite ? new Vector2(0, -900) : new Vector2(0, -900);  
                            break;
                        case Direction.Top:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(0, -900) : new Vector2(0, -900);  
                            break;
                        case Direction.Left:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(0, -900) : new Vector2(0, -900);  
                            break;
                        case Direction.Right:
                        
                            _slider.anchoredPosition = isOpposite ? new Vector2(0, -900) : new Vector2(0, -900);                          
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    _timer = 8;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}