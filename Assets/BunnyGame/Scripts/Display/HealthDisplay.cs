﻿using BunnyGame.Scripts.Static;
using UnityEngine;

namespace BunnyGame.Scripts.Display
{
    public class HealthDisplay : MonoBehaviour
    {
        private static float _angleOfHearts;

        private static GameObject _heart;
        private static Transform _heartContainer;
        private static RectTransform _rectTransform;
    
        private void Start()
        {
            //Assign values to the static variables.
            _heart = Resources.Load<GameObject>("Static-Loading/Heart Icon");
            _heartContainer = GameObject.FindWithTag("Heart-Container").transform;        

            var size = new Vector2(Screen.height * 0.25f, Screen.height * 0.25f);
            _rectTransform = GetComponent<RectTransform>();
            _rectTransform.sizeDelta = size;

            _angleOfHearts = 360f / GameState.GameLives;

            for (var i = 0; i < GameState.GameLives; i++)
            {
                var newHeart = Instantiate
                (
                    _heart,
                    Vector3.zero,
                    Quaternion.Euler(0, 0, i * _angleOfHearts), 
                    _heartContainer
                );

                newHeart.GetComponent<RectTransform>().anchoredPosition = new Vector2
                (
                    55 * Mathf.Cos(i * _angleOfHearts * Mathf.Deg2Rad),
                    55 * Mathf.Sin(i * _angleOfHearts * Mathf.Deg2Rad)
                );
            }
        }

        private void Update()
        {
            _heartContainer.Rotate(new Vector3(0, 0, -0.5f));

            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameState.ReduceHealth();
            }
        }

        public static void Refresh()
        {
            _angleOfHearts = 360f / GameState.GameLives;

            foreach (Transform h in _heartContainer)
            {
                Destroy(h.gameObject);
            }
        
            for (var i = 0; i < GameState.GameLives; i++)
            {
                var newHeart = Instantiate
                (
                    _heart,
                    Vector3.zero,
                    Quaternion.Euler(0, 0, (i * _angleOfHearts)), 
                    _heartContainer
                );

                newHeart.GetComponent<RectTransform>().anchoredPosition = new Vector2
                (
                    55 * Mathf.Cos(i * _angleOfHearts * Mathf.Deg2Rad),
                    55 * Mathf.Sin(i * _angleOfHearts * Mathf.Deg2Rad)
                );
            }
        }
    }
}