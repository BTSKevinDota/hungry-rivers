﻿using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace BunnyGame.Scripts.End_Game
{
    public class FoodWaterFall : MonoBehaviour
    {
        public Image[] foodPieces;
 
        public float timer;
        public float resetTimer;

        public float upForce;
        public float sideForce;
        public float gravityScale;

        public float dieTimer;

        private void Start()
        {
            timer = resetTimer;
        }
    
        private void Update()
        {
            if (timer <= 0)
            {
                var myTransform = transform;
                var food = Instantiate
                (
                    foodPieces[Random.Range(0, foodPieces.Length)],
                    myTransform.position, Quaternion.identity, 
                    myTransform
                );
                        
                food.GetComponent<Rigidbody2D>().AddForce(new Vector2(sideForce, upForce));
                food.GetComponent<Rigidbody2D>().gravityScale = gravityScale;

                Destroy(food.gameObject, dieTimer);
            
                timer = resetTimer;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
    }
}