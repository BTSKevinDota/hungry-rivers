﻿using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Bunny
{
    public class PlayerThree : PlayerBase
    {
        private PlayerThree()
        {
            keyMappings = new KeyMappings
            {
                axisHorizontal = "Horizontal3",
                axisVertical = "Vertical3",
                rotateClockwise =  KeyCode.G,
                rotateCounterClockwise = KeyCode.H,
                usePowerUp =  KeyCode.J
            };

            playerInput = new PlayerInput
            {
                input = Vector2.zero,
                oldInput = Vector2.zero
            };
            
            gameArea = new GameArea
            {
                pivot = new Vector2(0.5f, 0.5f), 
                spawn = new GameArea.Spawn
                {
                    position = Vector2.zero,
                    rotation = new Quaternion(0, 0, 0.7071068f, -0.7071068f)
                }          
            };
                    
            powerUp = new PowerUp
            {
                ready = false,
                timer = 0,
                resetTime = 15,
                inProgress = false,
                executingTime = 0
            };
            
            isHorizontal = true;
        }

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            BaseFallSpeed = Screen.height * 0.375f * Gravity;
            BaseHorizontalSpeed = Screen.height * 0.25f;
            gameArea.spawn.position = new Vector2(Screen.width * 0.5f - Screen.height * 0.5f, Screen.height * 0.5f);
        }

        protected override void Start()
        {
            base.Start();
            powerUp.timer = GameState.CooldownThree;
            powerUpIcon = Resources.Load<Sprite>("Sprites/Bunnies/Player3");
            powerUpIconActive = Resources.Load<Sprite>("Sprites/Bunnies/Player3Active");
            powerUpImage = GameObject.FindWithTag("PowerUpCooldown3").GetComponent<Image>();
        }

        protected override void OnValidate()
        {
        }
        
        #endregion

        #region Overrides

        protected override void Fall()
        {
            InputChange(new Vector2(GamePiece.GetVelocity().x, 0), new Vector2(VerticalMultiplier * BaseFallSpeed * Time.deltaTime, GamePiece.GetVelocity().y));
            
            GamePiece.ApplyForce(new Vector2(VerticalMultiplier * BaseFallSpeed * GameState.CurrentFallSpeedMultiplier *  Time.deltaTime,
                -playerInput.input.x * BaseHorizontalSpeed));
        }

        protected override void UsePowerUp()
        {           
            Destroy(Players.PlayerOne.GamePiece.gameObject);
            Destroy(Players.PlayerTwo.GamePiece.gameObject);
            Destroy(Players.PlayerThree.GamePiece.gameObject);
            Destroy(Players.PlayerFour.GamePiece.gameObject);
            
            GamePieceSpawner.SpawnFood(Players.PlayerOne);
            GamePieceSpawner.SpawnFood(Players.PlayerTwo);
            GamePieceSpawner.SpawnFood(Players.PlayerThree);
            GamePieceSpawner.SpawnFood(Players.PlayerFour);

            powerUp.inProgress = false;
            powerUp.timer = powerUp.resetTime;
        }

        #endregion
    }
}