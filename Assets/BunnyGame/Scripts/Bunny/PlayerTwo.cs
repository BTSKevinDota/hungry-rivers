﻿using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Bunny
{
    public class PlayerTwo : PlayerBase
    {
        private PlayerTwo()
        {
            keyMappings = new KeyMappings
            {
                axisHorizontal = "Horizontal2",
                axisVertical = "Vertical2",
                rotateClockwise =  KeyCode.T,
                rotateCounterClockwise = KeyCode.Z,
                usePowerUp =  KeyCode.U
            };

            playerInput = new PlayerInput
            {
                input = Vector2.zero,
                oldInput = Vector2.zero
            };
            
            gameArea = new GameArea
            {
                pivot = new Vector2(0.5f, 0.5f), 
                spawn = new GameArea.Spawn
                {
                    position = Vector2.zero,
                    rotation = new Quaternion(0,0,1,0)
                }             
            };        
            
            powerUp = new PowerUp
            {
                ready = false,
                timer = 0,
                resetTime = 3,
                inProgress = false,
                executingTime = 5f
            };
            
            isHorizontal = false;
        }
        
        #region Unity Methods
        
        protected override void Awake()
        {
            base.Awake();
            BaseFallSpeed = Screen.height * 0.375f * Gravity;
            BaseHorizontalSpeed = Screen.height * 0.25f;
            gameArea.spawn.position = new Vector2(Screen.width * 0.5f, Screen.height);
        }

        protected override void Start()
        {
            base.Start();
            powerUp.timer = GameState.CooldownTwo;
            powerUpIcon = Resources.Load<Sprite>("Sprites/Bunnies/Player2");
            powerUpIconActive = Resources.Load<Sprite>("Sprites/Bunnies/Player2Active");
            powerUpImage = GameObject.FindWithTag("PowerUpCooldown2").GetComponent<Image>();
        }

        protected override void Update()
        {
            base.Update();

            if (!powerUp.inProgress)
            {
                return;
            }
            
            if (powerUp.timeOfExecution < powerUp.executingTime)
            {
                Time.timeScale = 0.5f;
                powerUp.timeOfExecution += Time.unscaledDeltaTime;
                return;
            }

            powerUp.inProgress = false;
            powerUp.timer = powerUp.resetTime;
                
            Time.timeScale = 1;
        }

        protected override void OnValidate()
        {
        }
        
        #endregion

        #region Overrides

        protected override void Fall()
        {
            InputChange(new Vector2(0, GamePiece.GetVelocity().y), new Vector2(GamePiece.GetVelocity().x, -VerticalMultiplier * BaseFallSpeed * Time.deltaTime));                   
            GamePiece.ApplyForce(-new Vector2(playerInput.input.x * BaseHorizontalSpeed, VerticalMultiplier * BaseFallSpeed * GameState.CurrentFallSpeedMultiplier * Time.deltaTime));
        }

        protected override void UsePowerUp()
        {
            
        }

        #endregion
    }
}