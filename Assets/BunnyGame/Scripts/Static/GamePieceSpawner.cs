﻿using BunnyGame.Scripts.Bunny;
using BunnyGame.Scripts.Gameplay;
using UnityEngine;

namespace BunnyGame.Scripts.Static
{
    public static class GamePieceSpawner
    {
        private static readonly GamePiece[] BaseShapes;
        private static readonly GamePiece[] HardShapes;


        private const int HardShapeSpawnChance = 80;


        static GamePieceSpawner()
        {
            BaseShapes = Resources.LoadAll<GamePiece>("Food/BasicShape");        
            HardShapes = Resources.LoadAll<GamePiece>("Food/HardShape");        
        }

        public static void SpawnFood(PlayerBase player)
        {
            var spawnChance = Mathf.RoundToInt(Random.Range(0, 100));

            var gamePieces = spawnChance >= HardShapeSpawnChance ? HardShapes : BaseShapes;

            var randomPosition = player.isHorizontal ? new Vector2(0, Random.Range(-50, 50)) : new Vector2(Random.Range(-50, 50), 0);
           
            var newFood = Object.Instantiate
            ( 
                gamePieces[Random.Range(0, gamePieces.Length)],
                player.gameArea.spawn.position + randomPosition, 
                player.gameArea.spawn.rotation, 
                player.spawnContainer
            );
            
            player.AssignGamePiece(newFood);
            player.timeSinceLastSpawn = 0;
            newFood.SetOwner(player); 
        }
    }
}