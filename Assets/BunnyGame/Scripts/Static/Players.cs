﻿using BunnyGame.Scripts.Bunny;

namespace BunnyGame.Scripts.Static
{
    public static class Players
    {
        public static PlayerOne PlayerOne;
        public static PlayerTwo PlayerTwo;
        public static PlayerThree PlayerThree;
        public static PlayerFour PlayerFour;
    }
}