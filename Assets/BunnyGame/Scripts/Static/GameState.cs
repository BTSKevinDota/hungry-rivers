﻿using BunnyGame.Scripts.Display;
using UnityEngine.SceneManagement;

namespace BunnyGame.Scripts.Static
{
    public static class GameState
    {
        #region Variables

        #region Multipliers

        private const float StartFallSpeedMultiplier = 1f;
        private const float StartScoreMultiplier = 1f;

        private const float NextLevelFallMultiplier = 1.2f;
        private const float NextLevelScoreMultiplier = 1.1f;

        public static float CurrentFallSpeedMultiplier { get; private set; }
        public static float CurrentScoreMultiplier { get; private set; }

        #endregion
        
        public static int PlayerAmount;    
        
        public static int CurrentLevel { get; private set; }

        public static int GameLives { get;  private set; }

        public static float GameScore;


        public static float CooldownOne;
        public static float CooldownTwo;
        public static float CooldownThree;
        public static float CooldownFour;
        
         
        #endregion
                       
        public static void NewGame(int playerAmount)
        {
            CurrentFallSpeedMultiplier = StartFallSpeedMultiplier;
            CurrentScoreMultiplier = StartScoreMultiplier;
            
            CurrentLevel = 1;
            GameScore = 0;
            PlayerAmount = playerAmount;

            GameLives = playerAmount == 2 ? 4 : 8;
            
            CurrentFallSpeedMultiplier = StartFallSpeedMultiplier;
            CurrentScoreMultiplier = StartScoreMultiplier;

            CooldownOne = 0;
            CooldownTwo = 0;
            CooldownThree = 0;
            CooldownFour = 0;
            
            SceneManager.LoadScene("Transition"); 
        }

        public static void NextLevel()
        {
            CurrentLevel++;
            
            if (PlayerAmount == 2)
            {
                GameLives += 2;
            }
            else
            {
                GameLives += 4;
            }

            CurrentFallSpeedMultiplier *= NextLevelFallMultiplier;
            CurrentScoreMultiplier *= NextLevelScoreMultiplier;

            SceneManager.LoadScene("Transition");  
        }

        public static void ReduceHealth()
        {
            GameLives--;
            HealthDisplay.Refresh();
        }
    }
}